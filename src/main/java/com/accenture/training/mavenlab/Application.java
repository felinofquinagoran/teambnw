package com.accenture.training.mavenlab;

import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

@SpringBootApplication
@RestController
public class Application {

	/**
	 * @param args
	 */

	private ObjectMapper objectMapper = new ObjectMapper();

	public Application() {
		objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
	}

	@RequestMapping("/bong")
	public String bong() {
		return "bong";
	}

	@RequestMapping("/john")
	public String john() {
		return "John";
	}

	@RequestMapping("/warren")
	public String warren() {
		return "warren";
	}

	@RequestMapping(value = "/get-with-params", method = RequestMethod.POST)
	public String sampleQueryParam(@RequestParam Map<String, String> params) {
		return "You've hit the '/sample/get-with-params' request handler with the following query params:\n"
				+ params;
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		// COMMENTS BY WARREN
	}

}
